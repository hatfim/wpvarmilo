<?php
/**
 * Default config settings
 *
 * Enter any WordPress config settings that are default to all environments
 * in this file.
 *
 * Please note if you add constants in this file (i.e. define statements)
 * these cannot be overridden in environment config files so make sure these are only set once.
 *
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    2.0.0
 * @author     Studio 24 Ltd  <hello@studio24.net>
 */


/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3t|fj5CuW^J/%Ja7-i8J;S`]t9ek(!!lg=16kdU+JGPvo[)<bSG5~~*Go8(A|#K>');
define('SECURE_AUTH_KEY',  'y[[@[HUQ-4C]%nV%@Z!]#0H$#|I-`g7g}1{sdQ{B*9gnCm4d>d6-EvQT$fr I]m$');
define('LOGGED_IN_KEY',    'rl6c#UaO|}%y-93yH:M2&QuL6qypf|WWvQ;PFlj7MB&;)/]}K!Jx/mj bqe&dFk|');
define('NONCE_KEY',        'ZYq0o=Q=$HDP]]F6H_KKGj+-b!t9F(y6t-D/fji{Z3KCJsp[C)XeT- C G>2+E?#');
define('AUTH_SALT',        'vTKb{(A4lApM1:OD/:5-@(/0190i;dXXxODg2-wJ {d<|(b(p$-e+FsSUw1Oa-|j');
define('SECURE_AUTH_SALT', '/ng*+JDTp@a?2U8!6$K)hx.A!@VPN: `;iel47?|ms-fqLzkqi!i``u5,iM$9_)N');
define('LOGGED_IN_SALT',   'G/wE<D>9_AZ:>+wUU@~0(S}=b*P=LFDxp,|F)1R L1*HBz[)ZNMarA/HUzrRVcH|');
define('NONCE_SALT',       'NahK[8z//.eC.&>A7-F=mm+G9,SM/}LQ#$!=SVd9,qzFTdy)3}B2Bmxq)J3T]W{u');

/**#@-*/


/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'vr_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');


/**
 * Increase memory limit.
 */
define('WP_MEMORY_LIMIT', '64M');
