<?php

namespace App;

use Sober\Controller\Controller;
use WP_Query;

class FrontPage extends Controller
{

    public static function products($output)
    {
        switch ( $output )
        {
            case 'featured' :
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 4,
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'product_visibility',
                            'field'    => 'name',
                            'terms'    => 'featured',
                        ),
                    ),
                );
                break;
            case 'recent' :
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 4,
                );
                break;
            default;
                    return;
                break;
        }

        $query    = new WP_Query($args);
        return $query;
    }

    public function heroSlider() {
        $sliders = get_field('hero_slider', 'option');
        $data = [];

        if($sliders) {
            foreach($sliders as $slider)
            {
                $this_slider = (object) array(
                    'image' => $slider['image']['url'],
                    'link' => $slider['link'],
                );
                array_push($data, $this_slider);
            }

        }
        return $data;
    }

    public function bannerProduct() {
        $field = get_field('banner', 'option');
        $banner = (object) array(
            'image'=> $field['image']['url'],
            'headline'=> $field['headline'],
            'subheadline'=> $field['subheadline'],
            'button_text'=> $field['button_text'],
            'button_link'=> $field['link'],
        );

        return $banner;
    }
}
