<?php
if ( ! defined( 'ABSPATH' ) ) {
  exit;
}

?>
<form class="shop-ordering shop-sorting" method="get">
{{--   <div class="price-sorting">
    <h3>Price</h3>
    <div class="input-list">
      <input class="myorder input" id="sort-price" type="radio" name="myorder" value="price">
      <label for="sort-price">Low to High</label>
      |
      <input class="myorder input" id="sort-price-desc" type="radio" name="myorder" value="price-desc">
      <label for="sort-price-desc">High to Low</label>
    </div>
  </div> --}}
  <div class="select-sorting">
    <h3>Sort By</h3>

      <select name="orderby" class="orderby c-selectbox">
        <?php
          foreach ( $catalog_orderby_options as $id => $name ) :
        ?>
      <option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
        <?php endforeach; ?>
      </select>
      <input type="hidden" name="paged" value="1" />
      <?php wc_query_string_form_fields( null, array( 'orderby', 'submit', 'paged', 'product-page' ) ); ?>
  </div>
</form>
