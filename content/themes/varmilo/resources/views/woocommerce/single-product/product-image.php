<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;
if (method_exists($product, 'get_gallery_image_ids')) {
  $attachment_ids = $product->get_gallery_image_ids();
} else {
  $attachment_ids = $product->get_gallery_attachment_ids();
}

if ( $attachment_ids ) : ?>
  <div class="product__slider">
  <?php foreach ( $attachment_ids as $attachment_id ) : ?>
    <?php
      $med          = wp_get_attachment_image( $attachment_id, 'shop_single' );
      $image_link = wp_get_attachment_url( $attachment_id );
    ?>
    <div class="slider">
      <a href="<?php echo $image_link; ?>"><?php echo $med; ?></a>
    </div>
  <?php endforeach; ?>
  </div>
  <div class="product__slider__nav">
  <?php foreach ( $attachment_ids as $attachment_id ) : ?>
    <?php
      $image        = wp_get_attachment_image( $attachment_id, 'shop_thumbnail' );
    ?>
    <div class="slider__nav">
      <?php echo $image; ?>
    </div>
  <?php endforeach; ?>
  </div>
<?php endif; ?>
