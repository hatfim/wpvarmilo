@php
    global $product;
@endphp

<article @php(post_class('product__card'))>
    <div class="product__image">
        @include('WC::loop/sale-flash')
        {!! $product->get_image() !!}
        <div class="product__image__text">
            <a class="product__cta button button--outline" href="{!! $product->add_to_cart_url() !!}" class="btn btn-primary">Order Now</a>
        </div>
    </div>
    <div class="product__content">
        <p class="product__cat">{!! wc_get_product_category_list( $product->get_id() )  !!}</p>
        <h4 class="product__title">
            <a href="{{ get_permalink() }}">{!! $product->get_name() !!}</a>
        </h4>
        <p class="product__text">{!! $product->get_price_html() !!}</p>
    </div>
</article>
