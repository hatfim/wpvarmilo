@extends('layouts.shop')

@section('content')
    @if(have_posts())
        <div class="shop-active-filter">
            @php(dynamic_sidebar('sidebar-shop'))
        </div>
        <div class="shop-head">
          {{ !! woocommerce_result_count() }}
          {{ !! woocommerce_catalog_ordering() }}
        </div>
        <div class="product__list o-grid products">
            @while(have_posts()) @php(the_post())

                @php( do_action( 'woocommerce_shop_loop' ) )
                {{ App\wc_get_template_part('content', 'product', null, get_defined_vars()) }}

            @endwhile

        </div>
        @php( do_action( 'woocommerce_after_shop_loop' ) )
    @endif
@endsection
