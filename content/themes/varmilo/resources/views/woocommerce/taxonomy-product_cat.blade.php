{{--
    This file is the main page of the shop, so the overview of all products before choosing a category

    Here we look for a product template, named content-product.blade.php
    and then render this file while posts have posts
--}}

@extends('layouts.shop')


@section('content')
    <div class="shop-info">
        @php( do_action( 'woocommerce_before_shop_loop' ) )
    </div>

    <div class="shop-pagination">
        {{ !!  woocommerce_pagination() }}
    </div>
@endsection
