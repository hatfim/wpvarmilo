{{--
    This file acutally renders the product
    after you clicked on it from the main shop page.

    Here you can add your own magic & styling.

    So here we can finally use the wc_get_template function.

    This will load a template specific to the title for instance.

    App\wc_get_template('single-product/title.php') will look
    in the resources/views/woocommerce/single-product folder
    for a file named title.php and then render it.
--}}

@php
  global $product;
@endphp
<div class="o-grid product__detail--top">
  <div class="o-grid__cell u-1/1@mobile u-1/2@desktop">
    {!! woocommerce_show_product_images() !!}
  </div>
  <div class="o-grid__cell u-1/1@mobile u-1/2@desktop">
      <header>
          @include('WC::single-product/title')
          <p>SKU : {!! $product->get_sku() !!}</p>
          <p class="product__text">{!! $product->get_price_html() !!}</p>
      </header>
      <div class="product__content">
        <h3>Description</h3>
        {!! $post->post_excerpt !!}
        <p><a href="">Read More</a></p>
        <p><a class="product__cta button button--outline" href="{!! $product->add_to_cart_url() !!}" class="btn btn-primary">Order Now</a></p>
      </div>
  </div>
</div>
@php
  $specs = get_field('specifications');
@endphp
<div class="product__detail--bottom">
  <div class="tabs">
      <div class="tabbed-section__selector">
          <div class="tabbed-section__selector-tab-1 tab active" data-content="overview" data-pos="0">Overview</div>
          @if($specs)
          <div class="tabbed-section__selector-tab-2 tab" data-content="specifications" data-pos="180">Specifications</div>
          <span class="tabbed-section__highlighter highlighter"></span>
          @endif
      </div>
      <div class="tabbed-content">
        <div id="overview" class="tabbed visible">
            {{ the_field('overview') }}
        </div>
        @if($specs)
        <div id="specifications" class="tabbed hidden">
          {{ the_field('specifications') }}
        </div>
        @endif
      </div>
  </div>
</div>
