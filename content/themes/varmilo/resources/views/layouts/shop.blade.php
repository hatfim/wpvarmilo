<!doctype html>
<html @php(language_attributes())>
    @include('partials.head')
    <body @php(body_class())>
        @php(do_action('get_header'))
        @include('partials.header')
        <div id="shop" class=" shop shop-wrapper o-wrapper">
            <div id="content" class="site-content o-grid o-grid--reverse">
                <main id="main" class="main site-main o-grid__cell u-3/4@desktop u-2/3@tablet u-1/1@mobile">
                    @yield('content')
                </main>
                <aside id="secondary" class="sidebar o-grid__cell u-1/4@desktop u-1/3@tablet u-1/1@mobile widget-area" role="complementary">
                    @php(dynamic_sidebar('sidebar-primary'))
                </aside>
            </div>
        </div>
        @php(do_action('get_footer'))
        @include('partials.footer')
        @php(wp_footer())
    </body>
</html>
