<footer class="page-footer content-info">
  <div class="o-wrapper">
      <div class="o-grid">
        <div class="o-grid__cell u-1/3@tablet">
            <a class="brand" href="{{ home_url('/') }}">
                <img src="@asset('images/logo.svg')" alt="{{ get_bloginfo('name', 'display') }}" />
            </a>
        </div>
        <nav class="nav-bottom o-grid__cell u-1/3@tablet">
            <h3>Navigation</h3>
            @if (has_nav_menu('primary_navigation'))
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
            @endif
        </nav>
        <nav class="nav-social o-grid__cell u-1/3@tablet">
            <h3>Social</h3>
            <ul class="nav">
              <li class="menu-item"><a href="https://www.facebook.com/MechanicalKeyboardsID"><i class="icon icon-facebook"> </i>Facebook</a></li>
              <li class="menu-item"><a href="https://www.twitter.com/mechkeyboardsid"><i class="icon icon-twitter"></i>Twitter</a></li>
              <li class="menu-item"><a href="https://www.instagram.com/mechanicalkeyboards.co.id/"><i class="icon icon-instagram"> </i>Instagram</a></li>
              <li class="menu-item"><a href="https://www.youtube.com/channel/UC6dB-7Pb72CAFHBUwupt-lQ"><i class="icon icon-youtube"> </i>Youtube</a></li>
            </ul>
        </nav>
      </div>
  </div>
</footer>
