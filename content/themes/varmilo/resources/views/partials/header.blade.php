<header class="banner page-head">
    <div class="o-wrapper">
        <div class="o-grid o-grid--middle o-grid--between">
          <a class="brand" href="{{ home_url('/') }}">
              <img src="@asset('images/logo.svg')" alt="{{ get_bloginfo('name', 'display') }}" />
          </a>
          <nav class="nav-primary ">
               <div class="nav-mobile"><a id="nav-toggle" href="#!"><span></span></a></div>
              @if (has_nav_menu('primary_navigation'))
                  {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
              @endif
          </nav>
        </div>
    </div>
</header>
