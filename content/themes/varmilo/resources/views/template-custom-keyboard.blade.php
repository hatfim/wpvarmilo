{{--
  Template Name: Custom Keyboard Template
--}}

@extends('layouts.app')

@section('content')
  <div class="custom-iframe">
    <iframe id="iframe" src="https://varmilo-hendyzone.herokuapp.com" border="0" height="1200"></iframe>
  </div>

  <style type="text/css">
    .custom-iframe {
      max-width: 1200px;
      margin: 40px auto;
    }
    .custom-iframe iframe {
      width: 100%;
      border: 0;
      outline: 0;
      min-height: 800px;
    }
  </style>
@endsection
