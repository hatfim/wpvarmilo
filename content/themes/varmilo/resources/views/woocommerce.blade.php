{{--
  This file will call the woocommerce_content from setup with the get_definded_vars property.
  Don't know if it is necessary like the woocommerce_content from setup.php
--}}

@extends('layouts.app')


@section('content')
    {!! App\woocommerce_content(get_defined_vars()) !!}
@endsection
