@php
    $featured = FrontPage::products('featured');
    $recents = FrontPage::products('recent');
@endphp

@extends('layouts.app')

@section('content')
    {{-- Front Page Slider --}}
    @if($hero_slider)
        <section class="section hero home__slider slider">
            @foreach ($hero_slider as $slider)
                <div class="slide"><a href="{{ $slider->link }}"><img src="{{ $slider->image }}" /></a></div>
            @endforeach
        </section>
    @endif

    @if($featured)
        <section class="section home__product--featured o-wrapper">
            <header>
                <h2 class="headline">Featured Product</h2>
                <p class="subheadline">Introducing the unique products of Varmilo.</p>
            </header>
            <div class="product__list o-grid o-grid--around">
                @while ($featured->have_posts()) @php ($featured->the_post())
                    @include('woocommerce.content-product')
                @endwhile
            </div>
        </section>
    @endif

    @if($banner_product)
        <section class="section home__banner">
          <figure style="background-image: url({{ $banner_product->image }}); ">
              <figcaption class="o-wrapper">
                  <h2>{{ $banner_product->headline }}</h2>
                  <p>{{ $banner_product->subheadline }}</p>
                  <a class=" button button--outline" href="{{ $banner_product->button_link }}">{{ $banner_product->button_text }}</a>
              </figcaption>
            </figure>
        </section>
    @endif

    @if($recents)
        <section class="section home__product--recent o-wrapper">
            <header>
                <h2 class="headline">Recent Product</h2>
                <p class="subheadline">Introducing the unique products of Varmilo.</p>
            </header>
            <div class="product__list o-grid o-grid--around">
                @while ($recents->have_posts()) @php ($recents->the_post())
                    @include('woocommerce.content-product')
                @endwhile
            </div>
        </section>
    @endif



@endsection
