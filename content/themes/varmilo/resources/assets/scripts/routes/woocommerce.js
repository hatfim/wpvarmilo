export default {


  init() {
    jQuery(document).on('change', '.orderby', event => {
      const target = jQuery( event.currentTarget );
      const $form = target.parents('form');
      $form.submit();
      return false;
    });

  },

};
