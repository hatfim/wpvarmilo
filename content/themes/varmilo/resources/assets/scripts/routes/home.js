export default {
  init() {
    // JavaScript to be fired on the home page
  },
  finalize() {
    $('.slider').slick({
      arrows: false,
    });
    // JavaScript to be fired on the home page, after the init JS
  },
};
