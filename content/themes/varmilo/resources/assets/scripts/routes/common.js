import WebFont from'webfontloader';

export default {
  init() {
    WebFont.load({
        google: {
            families: ['Fira Sans:300,400,400i,600'],
        },
    });

    // JavaScript to be fired on all pages
  },
  finalize() {
    (function($) { // Begin jQuery
      $(function() { // DOM ready
        // Clicking away from dropdown will remove the dropdown class
        $('html').click(function() {
          $('.nav-dropdown').hide();
        });
        // Toggle open and close nav styles on click
        $('#nav-toggle').click(function() {
          $('ul.nav').slideToggle();
        });
        // Hamburger to X toggle
        $('#nav-toggle').on('click', function() {
          this.classList.toggle('active');
        });
      }); // end DOM ready
    })(jQuery);
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
