import classFunction from '../functions/class-selector';

export default {


  init() {
    this.productTabs();
  },

  finalize() {
     $('.product__slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      asNavFor: '.product__slider__nav',
    });
    $('.product__slider__nav').slick({
      slidesToShow: 8,
      slidesToScroll: 1,
      asNavFor: '.product__slider',
      dots: false,
      infinite: false,
      focusOnSelect: true,
    });
  },

  productTabs() {

    // Define tabs, write down them classes
    const tabs = document.getElementsByClassName('tab');
    const contents = document.getElementsByClassName('tabbed');

    // Create the toggle function
    const toggleTab = function(element) {
      // const parent = element.parentNode;
      element.addEventListener('click', (e) => {
        e.preventDefault();
        const $el = e.currentTarget;
        const link = $el.getAttribute('data-content');
        const $target = document.getElementById(link);
        for (let i = 0, len = tabs.length; i < len; i++) {
          classFunction.removeClass(tabs[i], 'active');
        }
        for (let i = 0, len = contents.length; i < len; i++) {
          if (classFunction.hasClass(contents[i], 'visible')) {
            classFunction.removeClass(contents[i], 'visible');
            classFunction.addClass(contents[i], 'hidden');
          }
        }

        classFunction.addClass($el, 'active');
        classFunction.removeClass($target, 'hidden');
        classFunction.addClass($target, 'visible');


      });
    }
    // Then finally, iterates through all tabs, to activate the
    // tabs system.
    for (let i = tabs.length - 1; i >= 0; i--) {
      toggleTab(tabs[i])
    }
  },
};
